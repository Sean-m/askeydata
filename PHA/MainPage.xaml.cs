﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Sensors;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PHA.DataModel;
using PHA.BaseClass;
using Windows.UI.Notifications;
using Windows.Storage;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;
using System.Text;
using Windows.Media.SpeechSynthesis;
using Windows.Media.Capture;
using Windows.UI.Xaml.Media.Imaging;

using Windows.Devices.Geolocation;
using Windows.System;
/// <summary>
/// Created by Sean on 14/06/2017
/// </summary>
namespace PHA
{
    public sealed partial class MainPage : Page
    {
        private List<SensorData_Gyroscope> lst_Gyro_Data = new List<SensorData_Gyroscope>();
        private List<SensorData_Accelerometer> lst_Acc_Data = new List<SensorData_Accelerometer>();
        private Accelerometer accelerometer;
        private Gyrometer gyroscope;
        private StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
        
        SendToServer sendToServer;

        private string deviceName;
        private string deviceId;

        public MainPage()
        {
            this.InitializeComponent();
            
            sendToServer = new SendToServer();

            // Get device info, we can use this info to identify the user
            deviceName = (new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation()).FriendlyName;
            deviceId = (new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation()).Id.ToString();

            accelerometer = Accelerometer.GetDefault();
            gyroscope = Gyrometer.GetDefault();
            rdi_fall.IsChecked = true;
        }

        private void appBarButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (btnStart_Icon.Symbol == Symbol.Play)
            {
                btnStart_Icon.Symbol = Symbol.Pause;
                startRecording();
            }
            else
            {
                btnStart_Icon.Symbol = Symbol.Play;
                stopRecording();
            }
        }

        private async void startRecording()
        {
			lst_Gyro_Data = new List<SensorData_Gyroscope>();
			lst_Acc_Data = new List<SensorData_Accelerometer>();
            status.Text = "startRecording";
            if (accelerometer != null)
            {
                accelerometer.ReportInterval = accelerometer.MinimumReportInterval > 60 ? accelerometer.MinimumReportInterval : 60;
                accelerometer.ReadingChanged += Accelerometer_ReadingChanged;
            }
            else
            {
                MessageDialog dialog = new MessageDialog("No accelerometer found");
                dialog.Title = "No Sensor Error";
                await dialog.ShowAsync();

            }

            if (gyroscope != null)
            {
                gyroscope.ReportInterval = gyroscope.MinimumReportInterval > 60 ? gyroscope.MinimumReportInterval : 60;
                gyroscope.ReadingChanged += Gyroscope_ReadingChanged;
            }
            else
            {
                MessageDialog dialog = new MessageDialog("No Gyroscope found");
                dialog.Title = "No Sensor Error";
                await dialog.ShowAsync();
            }
        }

        private void stopRecording()
        {
            status.Text = "stopRecording";
            if (accelerometer != null)
                accelerometer.ReadingChanged -= Accelerometer_ReadingChanged;
            if (gyroscope != null)
                gyroscope.ReadingChanged -= Gyroscope_ReadingChanged;

            storeData();
        }
        
        private void Gyroscope_ReadingChanged(Gyrometer sender, GyrometerReadingChangedEventArgs args)
        {
            GyrometerReading reading = args.Reading;
            lst_Gyro_Data.Add(new SensorData_Gyroscope(reading.AngularVelocityX, reading.AngularVelocityY, reading.AngularVelocityZ, reading.Timestamp.ToUnixTimeMilliseconds()));
        }
        
        private void Accelerometer_ReadingChanged(Accelerometer sender, AccelerometerReadingChangedEventArgs args)
        {
            AccelerometerReading reading = args.Reading;
            lst_Acc_Data.Add(new SensorData_Accelerometer(reading.AccelerationX, reading.AccelerationY, reading.AccelerationZ, reading.Timestamp.ToUnixTimeMilliseconds()));
        }

        private async void storeData()
        {
            StorageFile dataAccFile = await storageFolder.CreateFileAsync(deviceName + "_" + deviceId + "_" + action_name + "_dataAcc.txt", CreationCollisionOption.GenerateUniqueName);
            await FileIO.WriteTextAsync(dataAccFile, string.Join("", lst_Acc_Data));

            StorageFile dataGyroFile = await storageFolder.CreateFileAsync(deviceName + "_" + deviceId + "_" + action_name + "_dataGyro.txt", CreationCollisionOption.GenerateUniqueName);
            await FileIO.WriteTextAsync(dataGyroFile, string.Join("", lst_Gyro_Data));

            sendToServer.StoreInAzure(dataAccFile);
            sendToServer.StoreInAzure(dataGyroFile);
        }

        string action_name = "fall";
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if(((RadioButton)sender).Tag.Equals("fall"))
            {
                rdi_fall.IsChecked = true;
                rdi_other.IsChecked = false;
                action_name = "fall";
            }
            else
            {
                rdi_fall.IsChecked = false;
                rdi_other.IsChecked = true;
                action_name = "other";
            }
        }
    }

}
