﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Controls;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Windows.Storage;

namespace PHA.BaseClass
{
    class SendToServer
    {
        private static readonly HttpClient client = new HttpClient();
        public static string URL_API = "http://phademo.westeurope.cloudapp.azure.com/core/";

        private static string connectionString = "DefaultEndpointsProtocol=https;AccountName=wh1sandbox;AccountKey=sKhQdU+/1UD6lO0iZSayJP+5SFPXgxOuk+MiguYd0VXK4tmxFWUzoNbUQNDli366KrSsSYslu/V45sPMMqPgOA==;EndpointSuffix=core.windows.net";
        private CloudStorageAccount storageAccount;
        private CloudBlobClient blobClient;
        private CloudBlobContainer container;

        public SendToServer()
        {
            storageAccount = CloudStorageAccount.Parse(connectionString);
            blobClient = storageAccount.CreateCloudBlobClient();

            container = blobClient.GetContainerReference("askeydata");
            container.CreateIfNotExistsAsync();
            container.SetPermissionsAsync(
            new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
        }

        public void StoreInAzure(StorageFile data)
        {
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(data.Name);
            var t = Task.Run(() =>
            {
                var fileStream = File.OpenRead(data.Path);
                blockBlob.UploadFromFileAsync(data);
            });
            t.Wait();
        }
    }
}
