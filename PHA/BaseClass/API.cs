﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;

namespace PHA.BaseClass
{
    class API
    {
        static HttpClient httpClient = new HttpClient();
        static HttpResponseMessage httpResponse = new HttpResponseMessage();
        static string httpResponseBody = "";

        public static async void sendEvent(string workerId, string title, string desc, string longitude, string latitude)
        {
            try
            {
                string requestUri = "http://demo-sncf.westeurope.cloudapp.azure.com/api/event/declare/";

                JObject obj = new JObject
                {
                    { "worker_id", workerId },
                    { "title", title },
                    { "description", desc },
                    { "long", longitude },
                    { "lat", latitude }
                };

                HttpRequestMessage requestMessage = new HttpRequestMessage();
                requestMessage.Content = new StringContent(obj.ToString(), Encoding.UTF8, "application/json");

                httpResponse = await httpClient.PostAsync(requestUri, requestMessage.Content);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }
    }
}
