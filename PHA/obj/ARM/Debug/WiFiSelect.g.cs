﻿#pragma checksum "C:\UWP Projects\askey_data\PHA\WiFiSelect.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4D8750C73547A6286A9951A5E6CA4DC4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PHA
{
    partial class WifiSelect : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 1:
                {
                    this.appBarButtonBack = (global::Windows.UI.Xaml.Controls.AppBarButton)(target);
                    #line 72 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.AppBarButton)this.appBarButtonBack).Click += this.appBarButton_Click;
                    #line default
                }
                break;
            case 2:
                {
                    this.ConnectionBar = (global::Windows.UI.Xaml.Controls.ScrollViewer)(target);
                }
                break;
            case 3:
                {
                    this.ConnectButtons = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 4:
                {
                    this.NetworkKeyInfo = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 5:
                {
                    this.IsAutomaticReconnection = (global::Windows.UI.Xaml.Controls.CheckBox)(target);
                }
                break;
            case 6:
                {
                    global::Windows.UI.Xaml.Controls.Button element6 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 176 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element6).Click += this.ConnectButton_Click;
                    #line default
                }
                break;
            case 7:
                {
                    this.Keys1 = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 8:
                {
                    this.Keys2 = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 9:
                {
                    this.Keys3 = (global::Windows.UI.Xaml.Controls.StackPanel)(target);
                }
                break;
            case 10:
                {
                    global::Windows.UI.Xaml.Controls.Button element10 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 165 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element10).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 11:
                {
                    global::Windows.UI.Xaml.Controls.Button element11 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 166 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element11).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 12:
                {
                    global::Windows.UI.Xaml.Controls.Button element12 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 167 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element12).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 13:
                {
                    global::Windows.UI.Xaml.Controls.Button element13 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 168 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element13).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 14:
                {
                    global::Windows.UI.Xaml.Controls.Button element14 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 169 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element14).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 15:
                {
                    global::Windows.UI.Xaml.Controls.Button element15 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 170 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element15).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 16:
                {
                    global::Windows.UI.Xaml.Controls.Button element16 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 157 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element16).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 17:
                {
                    global::Windows.UI.Xaml.Controls.Button element17 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 158 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element17).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 18:
                {
                    global::Windows.UI.Xaml.Controls.Button element18 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 159 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element18).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 19:
                {
                    global::Windows.UI.Xaml.Controls.Button element19 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 160 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element19).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 20:
                {
                    global::Windows.UI.Xaml.Controls.Button element20 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 161 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element20).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 21:
                {
                    global::Windows.UI.Xaml.Controls.Button element21 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 162 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element21).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 22:
                {
                    global::Windows.UI.Xaml.Controls.Button element22 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 149 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element22).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 23:
                {
                    global::Windows.UI.Xaml.Controls.Button element23 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 150 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element23).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 24:
                {
                    global::Windows.UI.Xaml.Controls.Button element24 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 151 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element24).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 25:
                {
                    global::Windows.UI.Xaml.Controls.Button element25 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 152 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element25).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 26:
                {
                    global::Windows.UI.Xaml.Controls.Button element26 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 153 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element26).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 27:
                {
                    global::Windows.UI.Xaml.Controls.Button element27 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 154 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element27).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 28:
                {
                    global::Windows.UI.Xaml.Controls.Button element28 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 139 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element28).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 29:
                {
                    global::Windows.UI.Xaml.Controls.Button element29 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 140 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element29).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 30:
                {
                    global::Windows.UI.Xaml.Controls.Button element30 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 141 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element30).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 31:
                {
                    global::Windows.UI.Xaml.Controls.Button element31 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 142 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element31).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 32:
                {
                    global::Windows.UI.Xaml.Controls.Button element32 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 143 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element32).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 33:
                {
                    global::Windows.UI.Xaml.Controls.Button element33 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 144 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element33).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 34:
                {
                    global::Windows.UI.Xaml.Controls.Button element34 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 131 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element34).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 35:
                {
                    global::Windows.UI.Xaml.Controls.Button element35 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 132 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element35).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 36:
                {
                    global::Windows.UI.Xaml.Controls.Button element36 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 133 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element36).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 37:
                {
                    global::Windows.UI.Xaml.Controls.Button element37 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 134 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element37).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 38:
                {
                    global::Windows.UI.Xaml.Controls.Button element38 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 135 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element38).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 39:
                {
                    global::Windows.UI.Xaml.Controls.Button element39 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 136 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element39).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 40:
                {
                    global::Windows.UI.Xaml.Controls.Button element40 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 123 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element40).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 41:
                {
                    global::Windows.UI.Xaml.Controls.Button element41 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 124 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element41).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 42:
                {
                    global::Windows.UI.Xaml.Controls.Button element42 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 125 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element42).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 43:
                {
                    global::Windows.UI.Xaml.Controls.Button element43 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 126 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element43).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 44:
                {
                    global::Windows.UI.Xaml.Controls.Button element44 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 127 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element44).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 45:
                {
                    global::Windows.UI.Xaml.Controls.Button element45 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 128 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element45).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 46:
                {
                    global::Windows.UI.Xaml.Controls.Button element46 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 113 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element46).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 47:
                {
                    global::Windows.UI.Xaml.Controls.Button element47 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 114 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element47).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 48:
                {
                    global::Windows.UI.Xaml.Controls.Button element48 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 115 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element48).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 49:
                {
                    global::Windows.UI.Xaml.Controls.Button element49 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 116 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element49).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 50:
                {
                    global::Windows.UI.Xaml.Controls.Button element50 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 117 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element50).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 51:
                {
                    global::Windows.UI.Xaml.Controls.Button element51 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 118 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element51).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 52:
                {
                    global::Windows.UI.Xaml.Controls.Button element52 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 105 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element52).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 53:
                {
                    global::Windows.UI.Xaml.Controls.Button element53 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 106 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element53).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 54:
                {
                    global::Windows.UI.Xaml.Controls.Button element54 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 107 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element54).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 55:
                {
                    global::Windows.UI.Xaml.Controls.Button element55 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 108 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element55).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 56:
                {
                    global::Windows.UI.Xaml.Controls.Button element56 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 109 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element56).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 57:
                {
                    global::Windows.UI.Xaml.Controls.Button element57 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 110 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element57).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 58:
                {
                    global::Windows.UI.Xaml.Controls.Button element58 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 97 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element58).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 59:
                {
                    global::Windows.UI.Xaml.Controls.Button element59 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 98 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element59).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 60:
                {
                    global::Windows.UI.Xaml.Controls.Button element60 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 99 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element60).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 61:
                {
                    global::Windows.UI.Xaml.Controls.Button element61 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 100 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element61).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 62:
                {
                    global::Windows.UI.Xaml.Controls.Button element62 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 101 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element62).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 63:
                {
                    global::Windows.UI.Xaml.Controls.Button element63 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 102 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element63).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 64:
                {
                    this.NetworkKey = (global::Windows.UI.Xaml.Controls.PasswordBox)(target);
                }
                break;
            case 65:
                {
                    global::Windows.UI.Xaml.Controls.Button element65 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 92 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element65).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 66:
                {
                    global::Windows.UI.Xaml.Controls.Button element66 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 93 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element66).Click += this.keyboard_Click;
                    #line default
                }
                break;
            case 67:
                {
                    this.ResultsListView = (global::Windows.UI.Xaml.Controls.ListView)(target);
                    #line 79 "..\..\..\WiFiSelect.xaml"
                    ((global::Windows.UI.Xaml.Controls.ListView)this.ResultsListView).SelectionChanged += this.ResultsListView_SelectionChanged;
                    #line default
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

