﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PHA.DataModel
{
    public class SensorData_Accelerometer
    {
        public double Acc_X;
        public double Acc_Y;
        public double Acc_Z;
        public double time;

        public SensorData_Accelerometer(double Acc_X,double Acc_Y,double Acc_Z, double time)
        {
            this.Acc_X = Acc_X;
            this.Acc_Y = Acc_Y;
            this.Acc_Z = Acc_Z;
            this.time = time;
        }

        override
        public string ToString()
        {
            return "" + Acc_X +
                "," + Acc_Y +
                "," + Acc_Z +
                "," + time +
                "\n";
        }
    }
}