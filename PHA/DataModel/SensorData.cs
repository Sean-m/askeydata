﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PHA.DataModel
{
    class SensorData
    {
        public double Acc_X;
        public double Acc_Y;
        public double Acc_Z;
        public double Gyro_X;
        public double Gyro_Y;
        public double Gyro_Z;
        public double time;

        public SensorData(double Acc_X, double Acc_Y, double Acc_Z, double Gyro_X, double Gyro_Y, double Gyro_Z, double time)
        {
            this.Acc_X = Acc_X;
            this.Acc_Y = Acc_Y;
            this.Acc_Z = Acc_Z;
            this.Gyro_X = Gyro_X;
            this.Gyro_Y = Gyro_Y;
            this.Gyro_Z = Gyro_Z;
            this.time = time;
        }

        override
        public string ToString()
        {
            return "" + Acc_X +
                "," + Acc_Y +
                "," + Acc_Z + 
                "" + Gyro_X +
                "," + Gyro_Y +
                "," + Gyro_Z +
                "," + time +
                "\n";
        }
    }
}
