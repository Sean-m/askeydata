﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PHA.DataModel
{
    class SensorData_Gyroscope
    {
        public double Gyro_X;
        public double Gyro_Y;
        public double Gyro_Z;
        public double time;

        public SensorData_Gyroscope(double Gyro_X, double Gyro_Y, double Gyro_Z, double time)
        {
            this.Gyro_X = Gyro_X;
            this.Gyro_Y = Gyro_Y;
            this.Gyro_Z = Gyro_Z;
            this.time = time;
        }

        override
        public string ToString()
        {
            return "" + Gyro_X +
                "," + Gyro_Y +
                "," + Gyro_Z +
                "," + time +
                "\n";
        }
    }
}
