﻿using PHA.BaseClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.WiFi;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Connectivity;
using Windows.Security.Credentials;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace PHA
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WifiSelect : Page
    {
        public WifiSelect()
        {
            this.InitializeComponent();
        }
        private WiFiAdapter firstAdapter;
        public ObservableCollection<WiFiNetworkDisplay> ResultCollection
        {
            get;
            private set;
        }

        private void appBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (ConnectionBar.Visibility == Visibility.Visible)
                ConnectionBar.Visibility = Visibility.Collapsed;
            else
                Application.Current.Exit();
        }

        int index = 0;
        private void keyboard_Click(object sender, RoutedEventArgs e)
        {
            string currentButton = ((Button)sender).Tag.ToString();

            switch (currentButton)
            {
                case "space":
                    NetworkKey.Password += " ";
                    break;
                case "del":
                    if (NetworkKey.Password.Length > 0)
                        NetworkKey.Password = NetworkKey.Password.Substring(0, NetworkKey.Password.Length - 1);
                    break;
                case "back":
                    index--;
                    if (index < 0)
                        index = 2;
                    break;
                case "next":
                    index++;
                    if (index > 2)
                        index = 0;
                    break;
                default:
                    NetworkKey.Password += currentButton;
                    break;
            }
            switch(index)
            {
                case 0:
                    Keys1.Visibility = Visibility.Visible;
                    Keys2.Visibility = Visibility.Collapsed;
                    Keys3.Visibility = Visibility.Collapsed;
                    break;
                case 1:
                    Keys1.Visibility = Visibility.Collapsed;
                    Keys2.Visibility = Visibility.Visible;
                    Keys3.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    Keys1.Visibility = Visibility.Collapsed;
                    Keys2.Visibility = Visibility.Collapsed;
                    Keys3.Visibility = Visibility.Visible;
                    break;
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            ResultCollection = new ObservableCollection<WiFiNetworkDisplay>();

            // RequestAccessAsync must have been called at least once by the app before using the API
            // Calling it multiple times is fine but not necessary
            // RequestAccessAsync must be called from the UI thread
            var access = await WiFiAdapter.RequestAccessAsync();
            if (access != WiFiAccessStatus.Allowed)
            {
                //App.ViewModel.currentStatusText = "WiFI: Access denied";
                return;
            }
            else
            {
                DataContext = this;

                var result = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(WiFiAdapter.GetDeviceSelector());
                if (result.Count >= 1)
                {
                    firstAdapter = await WiFiAdapter.FromIdAsync(result[0].Id);
                }
                else
                {
                    //App.ViewModel.currentStatusText = "WiFI: No Network Adapter found";
                    return;
                }
            }

            await firstAdapter.ScanAsync();
            ConnectionBar.Visibility = Visibility.Collapsed;
            DisplayNetworkReport(firstAdapter.NetworkReport);
        }

        private void DisplayNetworkReport(WiFiNetworkReport report)
        {
            ResultCollection.Clear();

            foreach (var network in report.AvailableNetworks)
            {
                ResultCollection.Add(new WiFiNetworkDisplay(network, firstAdapter));
            }
        }


        private void ResultsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedNetwork = ResultsListView.SelectedItem as WiFiNetworkDisplay;
            if (selectedNetwork == null)
            {
                return;
            }

            // Show the connection bar
            ConnectionBar.Visibility = Visibility.Visible;

            // Only show the password box if needed
            if (selectedNetwork.AvailableNetwork.SecuritySettings.NetworkAuthenticationType == NetworkAuthenticationType.Open80211)
            {
                NetworkKeyInfo.Visibility = Visibility.Collapsed;
            }
            else
            {
                NetworkKeyInfo.Visibility = Visibility.Visible;
                //ConnectButtons.Children.Add(new KeyInput());
            }
        }

        private async void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedNetwork = ResultsListView.SelectedItem as WiFiNetworkDisplay;
            if (selectedNetwork == null || firstAdapter == null)
            {
                return;
            }
            WiFiReconnectionKind reconnectionKind = WiFiReconnectionKind.Manual;
            if (IsAutomaticReconnection.IsChecked.HasValue && IsAutomaticReconnection.IsChecked == true)
            {
                reconnectionKind = WiFiReconnectionKind.Automatic;
            }

            WiFiConnectionResult result;
            if (selectedNetwork.AvailableNetwork.SecuritySettings.NetworkAuthenticationType == Windows.Networking.Connectivity.NetworkAuthenticationType.Open80211)
            {
                result = await firstAdapter.ConnectAsync(selectedNetwork.AvailableNetwork, reconnectionKind);
            }
            else
            {
                // Only the password potion of the credential need to be supplied
                var credential = new PasswordCredential();
                credential.Password = NetworkKey.Password;

                result = await firstAdapter.ConnectAsync(selectedNetwork.AvailableNetwork, reconnectionKind, credential);
            }

            if (result.ConnectionStatus == WiFiConnectionStatus.Success)
            {
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(MainPage), e);

            }
            else
            {
                //                rootPage.NotifyUser(string.Format("Could not connect to {0}. Error: {1}", selectedNetwork.Ssid, result.ConnectionStatus), NotifyType.ErrorMessage);
            }

            // Since a connection attempt was made, update the connectivity level displayed for each
            foreach (var network in ResultCollection)
            {
                network.UpdateConnectivityLevel();
            }

        }
    }
}
